export default {
    'Subscription': { id: {type: String, index: true}, resource: String, expirationDateTime: String, AuthToken: String, RefreshToken: String }
}