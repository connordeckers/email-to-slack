import env from 'dotenv'; env.config();
import https from 'https';
import url from 'url';
import debug from 'debug';

const log = debug('PostToSlack');
const options = { ...url.parse(process.env.SLACK_HOOK), method: 'POST' }
export const Slack = {
    post(data) {
        const req = https.request(
            { ...options, headers: { 'Content-Type': 'application/json', 'Content-Length': Buffer.byteLength(JSON.stringify(data)) } }, 
            (res) => { res.setEncoding('utf8'); res.on('end', () => log('Data sent.')); }
        );
        req.on('error', (e) => { log(`Problem with request: ${e.message}`); });
        req.write(JSON.stringify(data));
        req.end();
    }
}