import './DB';
import env from 'dotenv'; env.config();
import express from 'express';
import debug from 'debug';
import routes from './Routes';
import https from 'https';
import fs from 'fs';

const PORT = process.env.PORT || 3000;
const app = new express();
routes(app);

const {FULLCHAIN, PRIVKEY} = process.env;

// app.listen(PORT, () => debug('Express')(`Express listening on port ${PORT}`));

/**
 * Create HTTP server.
 */
var server = https.createServer({
    cert: fs.readFileSync(FULLCHAIN),
    key: fs.readFileSync(PRIVKEY)
}, app);

/**
 * Listen on provided port, on all network interfaces.
 */
app.set('port', PORT);
server.listen(PORT);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
    if (error.syscall !== 'listen') { throw error; }
    var bind = typeof PORT === 'string' ? 'Pipe ' + PORT : 'Port ' + PORT;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    var addr = server.address();
    var bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
    debug('Express')(`Express listening on port ${PORT}`)
}