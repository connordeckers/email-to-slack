import env from 'dotenv'; env.config();
import express from 'express';
import debug from 'debug';
import * as graph from '@microsoft/microsoft-graph-client';
import * as Auth from '../Auth';
import { Schema } from '../DB';
import { Rules } from '../Rules';

import TestMessage from '../TestEmail.json';

const {Subscription} = Schema;
const ONE_MINUTE = 1000 * 60;
const log = debug('ExpressRequest');

const processNotification = (msg) => new Promise(async (res, rej) => {
    try {
        debug('NotificationProcessor')('Message received');
        if (msg.clientState == process.env.APP_SECRET) {
            let subscription = await Subscription.findOne({ id: msg.subscriptionId }).exec();
            if(subscription) {
                let token = subscription.AuthToken;
        
                const client = graph.Client.init({ debugLogging: true, authProvider: (done) => done(null, token) });
                client.api(`https://graph.microsoft.com/beta/${msg.resource}`).get((err, message) => {
                    if (err) { debug('NotificationProcessor')('Message request error: ', err); rej(err); } 
                    else { Rules.process(message); res('Message parsed and processed.'); }
                });
            } else {
                console.log(subscription, msg);
            }
        }
    } catch(e) {
        log('ERROR: ', e);
    }
});

const API_BASE = `https://graph.microsoft.com/beta`;

export const Subscriptions = () => {
    const route = express.Router();

    route.get('/test', (req, res) => {
        Rules.process(TestMessage, true)
        res.send('');
	});

	route.get('/list', async (req, res) => {
        const accessToken = await Auth.getAccessToken(req.cookies, res);
        if (accessToken) {
            const client = graph.Client.init({ debugLogging: true, authProvider: (done) => { done(null, accessToken); } });
			client.api(API_BASE+'/subscriptions').get((err, r) => res.send(err||r||'No data found.'));
		} else {
			res.send('Not signed in.');
		}
	});

    route.post('/notify', async (req, res) => {
        // console.log(req.body);
        if (req.query.validationToken) { res.send(req.query.validationToken); }
        else { res.sendStatus(202); req.body.value.map(processNotification); }
    });

    route.get('/subscribe', async (req, res) => {
        const accessToken = await Auth.getAccessToken(req.cookies, res);
        if (accessToken) {
            const client = graph.Client.init({ debugLogging: true, authProvider: (done) => { done(null, accessToken); } });

            let d = new Date();
            d.setTime(d.getTime() + 4200 * ONE_MINUTE);

            client
                .api(API_BASE + '/subscriptions')
                .post({
                    "changeType": "created",
                    "notificationUrl": process.env.NOTIFY_URI,
                    "resource": "/me/messages",
                    "expirationDateTime": d.toISOString(),
                    "clientState": process.env.APP_SECRET
                }, async (err, r) => {
                    if (err) return log('Error: ', err);
                    if (r && r.clientState == process.env.APP_SECRET) {
                        r.AuthToken = accessToken;
                        r.RefreshToken = req.cookies.graph_refresh_token;
                        let subscription = new Subscription(r);
                        try {
                            let r = await subscription.save();
                            log('Subscription saved.');
                        } catch (e) {
                            log(e);
                        }
                    }
                    else log(r);
                    res.redirect('/');
                });
        } else {
            res.send('Not signed in?');
        }
    });

    route.get('/renew', async (req, res) => {
        try {
            let items = await Subscription.find({}).exec();
            items.forEach(item => {
                const client = graph.Client.init({ debugLogging: true, authProvider: (done) => { done(null, item.AuthToken); } });
                let newTime = new Date(Date.now() + 4200 * ONE_MINUTE);
                client.api(`${API_BASE}/subscriptions/${item.id}`).patch({ expirationDateTime: newTime }, (err, r) => {
                    if (err) return log(err);
                    Subscription.updateOne({ id: item.id }, { expirationDateTime: r.expirationDateTime }, (err) => {
                        if (err) log(err);
                        else log('Subscription %s updated from %s to %s', item.id, item.expirationDateTime, r.expirationDateTime);
                    });
                });
            });
        } catch(e) {
            log(e);
        }
        res.send('');
    });

    route.get('/delete/:subscriptionID', async (req, res) => {
        const accessToken = await Auth.getAccessToken(req.cookies, res);
        if (accessToken) {
            const client = graph.Client.init({ debugLogging: true, authProvider: (done) => { done(null, accessToken); } });
            client
                .api(`${API_BASE}/subscriptions/${req.params.subscriptionID}`)
                .delete((error1, r) => {
                    Subscription.deleteOne({ id: req.params.subscriptionID }, error2 => {
                        if (error1 || error2) { log(error1); log(error2); return res.send([error1, error2]); }
                        else {
                            // res.send(`Deleted ${req.params.subscriptionID} successfully.`); 
                            res.redirect('/');
                        }
                    });
                });
        } else { res.send('Not signed in?'); }
    });

    return route;
}