import fs from 'fs';
import path from 'path';
import debug from 'debug';

let rules = [];
export const Rules = new class {
    constructor() { 
        const GetFiles = (dir) => new Promise((res, rej) => fs.readdir(dir, (e, f) => { if (e) { rej(e) } else { res(f.map(fn => path.join(dir, fn))) } }));
        GetFiles('./rules').then(files => files.forEach(file => rules.push(require('./' + file).default)));
    }

    process(msg, testonly=false) {
        try {
            const StringToKey = (base, string) => string.split('.').reduce((obj, key) => obj[key], base);
            const MatchesRules = (rules) => {
                const keys = Object.keys(rules);
                for(let i=0; i<keys.length; i++) {
                    const reg =  new RegExp(rules[keys[i]].replace(/\*/g, '.*'), 'gi');
                    const match = StringToKey(msg, keys[i]);
                    if((match.match(reg)||[]).length == 0) return false;
                }
                return true;
            }
			rules.forEach(rule => { 
				if(testonly) debug('RuleProcessor')(MatchesRules(rule.rules));
				else if(MatchesRules(rule.rules)) rule.output(rule.process(msg)); 
			});
        } catch(e) {
            debug('RulesProcessor')(e);
        }
    }
}