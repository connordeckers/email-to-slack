import mongoose from 'mongoose';
import Schemas from './Schemas';
import debug from 'debug';

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/subscriptions', { useNewUrlParser: true });
const db = mongoose.connection;
db.on('error', (console.error.bind(console, 'connection error:')));
db.once('open', () => debug('MongoDB')('Mongoose connection active'));

let schemas = {
    register(name, schema) {
        debug('MongoSchema')('Registered schema ' + name);
        this[name] = mongoose.model(name, new mongoose.Schema(schema));
    }
};

export const Schema = schemas;
Object.keys(Schemas).forEach(name => Schema.register(name, Schemas[name]));