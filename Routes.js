import env from 'dotenv'; env.config();
import debug from 'debug';
import express from 'express';
import cookieParser from 'cookie-parser';
import * as Auth from './Auth';
import {Schema} from './DB';
import { Debug } from './routes/Debugging';
import { OAuth } from './routes/OAuth';
import { Subscriptions } from './routes/Subscription';

const log = debug('ExpressRequest');
const logger = (req, res, next) => { log(`${req.method}: ${req.url}`); next(); }

export default (app) => {
    app.use(logger);
    app.use(express.json());
    app.use(express.urlencoded({ extended: false }));
    app.use(cookieParser());

    app.get('/', async (req, res) => {
        const accessToken = await Auth.getAccessToken(req.cookies, res);
        const userName = req.cookies.graph_user_name;
        const userEmail = req.cookies.graph_user_email;

        let html = [];
        html.push(`Signed into Outlook as: ${userName} &lt;${userEmail}&gt;`);
        

        
        let subscriptions = await Schema.Subscription.find({}).exec();
        if (subscriptions.length > 0) {
            let list = `<ul>`
            subscriptions.forEach(sub => list += `<li>${sub.id} <a href="/subscription/delete/${sub.id}">[delete]</li>`)
            list += '</ul>';
            html.push('Active connections: <br>' + list);
        } else {
            html.push('No subscriptions found. <a href="/subscription/subscribe">Click here to subscribe.</a>');
        }

        html.push('<a href="/auth/signout">Sign out</a>')

        if (accessToken && userName) {
            res.send(html.join('<br><br>'));
        } else {
            res.send(`Not signed in. <a href="/auth/signin">Sign In</a>`);
        }
    });

    app.use('/auth', OAuth());
    app.use('/subscription', Subscriptions());
    app.use('/debug', Debug());
}