import { Slack } from '../PostToSlack';

export default {
    "rules": {
        "from.emailAddress.address": "donotreply.mylo@utas.edu.au",
        "subject": "*(created the thread|replied to)*",
        "body.content": "*Technical Questions*",
    },
    "process": (msg) => {
        const regex = /<h1[^]+?>([^]+?)<\/h1>[^]+?<div[^]+?>([^]+?)<\/div>[^]*?<a.*?href="(.*?\/d2l\/lms\/discussions\/messageLists\/.*?)".*?>View (Thread|Reply)<\/a>[^]+?<a href="https:\/\/mylo\.utas\.edu\.au\/d2l\/p\/home\/[^]+?>([^]+?)<\/a>/i;
        const [, subject, post, link, ,unit] = msg.body.content.match(regex).map(txt => txt.replace(/<.*?>/gi, '').replace(/&nbsp;/gi, '').trim());
        return { subject, post, link, unit, ts: msg.sentDateTime };
    },
    "output": (args) => {
        const {subject, post, link, unit, ts} = args;
        // const short = post.replace(/\s/g, ' ').replace(/\s+/g, ' ');
				const [, name, action, thread] = /(.*)(created the thread|replied to)(.*)/gi.exec(subject).map(text => text.trim());
				const colour = action.includes('replied') ? '#ffc300' : '#36a64f';
        Slack.post({
            "attachments": [
                {
                    "fallback": subject,
                    "color": colour,
                    "pretext": subject,
                    "author_name": name,
                    "title": thread,
                    "title_link": link.replace(/&amp;/gi, '&'),
                    "text": post,
                    "footer": unit,
                    "ts": (new Date(ts)).getTime() / 1000
                }
            ]
        })
    }
}