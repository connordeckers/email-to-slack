import express from 'express';
import debug from 'debug';
import * as Auth from '../Auth';
import { Schema } from '../DB';

const { Subscription } = Schema;
const log = debug('ExpressRequest');

export const OAuth = () => {
    const route = express.Router();
    route.get('/signin', async (req, res) => {
        const accessToken = await Auth.getAccessToken(req.cookies, res);
        if (accessToken) res.redirect('/');
        else res.redirect(Auth.getAuthUrl());
    });

    route.get('/authorize', async (req, res) => {
        const code = req.query.code;
        if (code) {
            try { await Auth.getTokenFromCode(code, res); res.redirect('/'); }
            catch (error) { res.send('Error: ' + error); }
        } else res.send('Authorization error: Missing code parameter');
    });

    route.get('/refresh', async (req, res) => {
        const items = await Subscription.find({}).exec();
        const resp = await Promise.all(items.map(async item => {
            let resp = await Auth.getRefreshedToken(item.RefreshToken);
            Subscription.updateOne({id: item.id}, {AuthToken: resp.token.access_token, RefreshToken: resp.token.refresh_token}, (err) => {
                if(err) console.log('Renewal error: ', err);
                else log('User auth refreshed.');
            });
            return resp;
        }));
        res.send(resp);
    });

    route.get('/signout', (req, res) => { Auth.clearCookies(res); res.redirect('/'); });
    
    return route;
}