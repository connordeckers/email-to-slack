import env from 'dotenv'; env.config();
import jwt from 'jsonwebtoken';
import oauth2 from 'simple-oauth2';

const { APP_ID, APP_PASSWORD, APP_SCOPES, REDIRECT_URI } = process.env; 
const credentials = {
    client: { id: APP_ID, secret: APP_PASSWORD, }, 
    auth: { tokenHost: 'https://login.microsoftonline.com', authorizePath: 'common/oauth2/v2.0/authorize', tokenPath: 'common/oauth2/v2.0/token' }
};

const oauth = oauth2.create(credentials);

export const getAuthUrl = () => oauth.authorizationCode.authorizeURL({ redirect_uri: REDIRECT_URI, scope: APP_SCOPES });
export const getTokenFromCode = async (auth_code, res) => {
    const result = await oauth.authorizationCode.getToken({ code: auth_code, redirect_uri: REDIRECT_URI, scope: APP_SCOPES });
    const token = oauth.accessToken.create(result);
    saveToCookies(token,res);
    return token.token.access_token;
}

export const getRefreshedToken = async (refresh_token) => await oauth.accessToken.create({refresh_token}).refresh();

export const saveToCookies = (token, res) => {
    // Parse the identity token
    const user = jwt.decode(token.token.id_token);

    // Save the access token in a cookie
    res.cookie('graph_access_token', token.token.access_token, { maxAge: 3600000, httpOnly: true });
    // Save the user's name in a cookie
    res.cookie('graph_user_name', user.name, { maxAge: 3600000, httpOnly: true });
    // Save the user's email in a cookie
    res.cookie('graph_user_email', user.preferred_username, { maxAge: 3600000, httpOnly: true });
    // Save the refresh token in a cookie
    res.cookie('graph_refresh_token', token.token.refresh_token, { maxAge: 7200000, httpOnly: true });
    // Save the token expiration time in a cookie
    res.cookie('graph_token_expires', token.token.expires_at.getTime(), { maxAge: 3600000, httpOnly: true });
}

export const clearCookies = (res) => {
    // Clear cookies
    res.clearCookie('graph_access_token', { maxAge: 3600000, httpOnly: true });
    res.clearCookie('graph_user_name', { maxAge: 3600000, httpOnly: true });
    res.clearCookie('graph_refresh_token', { maxAge: 7200000, httpOnly: true });
    res.clearCookie('graph_token_expires', { maxAge: 3600000, httpOnly: true });
}

export const getAccessToken = async (cookies, res) => {
    // Do we have an access token cached?
    let token = cookies.graph_access_token;

    if (token) {
        // We have a token, but is it expired?
        // Expire 5 minutes early to account for clock differences
        const FIVE_MINUTES = 300000;
        const expiration = new Date(parseFloat(cookies.graph_token_expires - FIVE_MINUTES));
        if (expiration > new Date()) {
            // Token is still good, just return it
            return token;
        }
    }

    // Either no token or it's expired, do we have a
    // refresh token?
    const refresh_token = cookies.graph_refresh_token;
    if (refresh_token) {
        const newToken = await oauth.accessToken.create({ refresh_token: refresh_token }).refresh();
        saveToCookies(newToken, res);
        return newToken.token.access_token;
    }

    // Nothing in the cookies that helps, return empty
    return null;
}