import express from 'express';
import {Rules} from '../Rules.js';
import msg from '../TestEmail.json';
import { Schema } from '../DB';

const API_BASE = `https://graph.microsoft.com/beta`;
export const Debug = () => {
    const route = express.Router();

    route.get('/message', async (req, res) => {
        const accessToken = await Auth.getAccessToken(req.cookies, res);
        if (accessToken) {
            const client = graph.Client.init({ debugLogging: true, authProvider: (done) => { done(null, accessToken); } });
            client.api(`${API_BASE}/${req.query.msg}`).get((err, message) => {
                if (err) { res.send(err); }
                else { res.send(message); }
            });
        } else {
            res.send('Not logged in.');
        }
    });

    route.get('/flush', (req, res) => {
        Schema.Subscription.remove({}, (err) => {
            if(err) res.send(err);
            else res.send('Database flushed.');
        });
    });

    route.get('/testrules', async (req, res) => {
        Rules.process(msg)
        res.send('Check the console.');
    });

    return route;
}